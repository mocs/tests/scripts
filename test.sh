#!/usr/bin/env bash

# Use this file to test _this_ repository

# Always die on error
set -e

# Get the folder name of _this_ script
SRCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Get the folder of the executing instance
TARGET=$(pwd)

# Get common stuff
source ${SRCDIR}/scripts/common/shell.sh

printf "${RED}"
message "ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION"
printf "${NC}"
printf "${red}"
message "These tests are temporary! They SHALL be replaced by the test suite!"
printf "${NC}"

read -n1 -r -p "Press any key to continue..." key

# Execute tests
#LINE_WIDTH=120
printf "${MAGENTA}"
message "Executing tests"
printf "${NC}"

printf "Testing function ${CYAN}format_time${NC}\n"
format_time $(date +%s.%N | tr 'N' '0')
printf "${GREEN}Done\n\n"


printf "Testing function ${CYAN}percentage${NC}\n"
percentage 1 1
percentage 4 3
percentage 3.4 1.3
printf "${GREEN}Done\n\n"

printf "Testing function ${CYAN}string_length${NC}\n"
test_string="0 1 2 3 4 5 6 7 8 9"
printf "${test_string}:$(string_length "${test_string}")\n"
test_string="0 1 2 3 4 5 ${BLUE}6 7 8 9"
printf "${test_string}:$(string_length "${test_string}")\n"
test_string="0 1 2 3 4 5 ${cyan}6 7 8 9"
printf "${test_string}:$(string_length "${test_string}")\n"
test_string="0 1 2 3 4 5 6${NC} 7 8 9"
printf "${test_string}:$(string_length "${test_string}")\n"
test_string="0 1 2 3 4 5 ${cyan}6${NC} 7 8 9"
printf "${test_string}:$(string_length "${test_string}")\n"
printf "${GREEN}Done\n\n"

printf "Testing function ${CYAN}split_text${NC}\n"
printf "$(split_text "Testing a short message. Right?" 10)\n"
printf "$(split_text "Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message. Testing a a super long message.

Testing a a super long message.
Testing a a super long message.
§ § § §
AFTER SPACE. Testing a a super long message. Testing a a super long message. ")\n"
printf "$(split_text "0 1 2 3 4 5 6 7 8 9" 5)\n"
printf "$(split_text "0 1 2 3 4 5${NC} 6 7 8 9" 5)\n"
printf "$(split_text "0 1 2 3 4 5 6 ${BLUE}7 8 9" 5)\n"
printf "${GREEN}Done\n\n"


printf "Testing function ${CYAN}message${NC}\n"
message "Testing a short message no color"
printf "${BLUE}"
message "Testing a short message in ${cyan}color${BLUE}"
printf "${NC}"
message "Testing a long message. Testing a long message. Testing a long message. Testing a long message. Testing a long message. Testing a long message. Testing a long message. "
printf "${GREEN}Done\n\n"


printf "Testing function ${CYAN}ProgressBar${NC}\n"
ProgressBar 1 0
echo ""
ProgressBar 24 12
echo ""
ProgressBar 0 10
echo ""
ProgressBar 1 10
echo ""
ProgressBar 7 24
echo ""
ProgressBar 77 78
echo ""
ProgressBar 78 78
echo ""
for (( i = 0; i <= 300; i++ )); do
  ProgressBar $i 300
done
echo ""
printf "${GREEN}Done\n\n"

printf "Testing function ${CYAN}num_print${NC}\n"
num_print 2345192.424
echo ""
printf "${GREEN}Done\n\n"


#Exit
printf "${MAGENTA}"
message "All done!"
printf "${NC}"
