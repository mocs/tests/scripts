#!/usr/bin/env bats
load ../helpers/bin_helper

@test "copy_folder_gently::::Testing that no arguments fails." {
  run copy_folder_gently.sh
  expected=$(printf "${RED}You must supply exactly two arguments.
${cyan}First must be a folder.
Second must be a folder.
${NC}
${RED}################################################################################
## Execution failed! Please review any error messages!                        ##
################################################################################
${NC}")
  echo "expected = '${expected[@]}'"
  echo "Output   = '${output[@]}'"
  echo "Status   = '${status}'"
  [ "$status" -eq 1 ]
  [ "$output" = "$expected" ]
}
