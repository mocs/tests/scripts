#!/usr/bin/env bats
load ../helpers/color_helper

# Tests for colors.sh
@test "color::::Test that BLACK is printed correctly." {
  # Execute test
  run echo "${BLACK}BLACK WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;30mBLACK WITH space" ]
}

@test "color::::Test that RED is printed correctly." {
  # Execute test
  run echo "${RED}RED WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;31mRED WITH space" ]
}

@test "color::::Test that GREEN is printed correctly." {
  # Execute test
  run echo "${GREEN}GREEN WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;32mGREEN WITH space" ]
}

@test "color::::Test that YELLOW is printed correctly." {
  # Execute test
  run echo "${YELLOW}YELLOW WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;33mYELLOW WITH space" ]
}

@test "color::::Test that BLUE is printed correctly." {
  # Execute test
  run echo "${BLUE}BLUE WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;34mBLUE WITH space" ]
}

@test "color::::Test that MAGENTA is printed correctly." {
  # Execute test
  run echo "${MAGENTA}MAGENTA WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;35mMAGENTA WITH space" ]
}

@test "color::::Test that CYAN is printed correctly." {
  # Execute test
  run echo "${CYAN}CYAN WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;36mCYAN WITH space" ]
}

@test "color::::Test that WHITE is printed correctly." {
  # Execute test
  run echo "${WHITE}WHITE WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;37mWHITE WITH space" ]
}
@test "color::::Test that black is printed correctly." {
  # Execute test
  run echo "${black}black WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;30mblack WITH space" ]
}

@test "color::::Test that red is printed correctly." {
  # Execute test
  run echo "${red}red WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;31mred WITH space" ]
}

@test "color::::Test that green is printed correctly." {
  # Execute test
  run echo "${green}green WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;32mgreen WITH space" ]
}

@test "color::::Test that yellow is printed correctly." {
  # Execute test
  run echo "${yellow}yellow WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;33myellow WITH space" ]
}

@test "color::::Test that blue is printed correctly." {
  # Execute test
  run echo "${blue}blue WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;34mblue WITH space" ]
}

@test "color::::Test that magenta is printed correctly." {
  # Execute test
  run echo "${magenta}magenta WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;35mmagenta WITH space" ]
}

@test "color::::Test that cyan is printed correctly." {
  # Execute test
  run echo "${cyan}cyan WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;36mcyan WITH space" ]
}

@test "color::::Test that white is printed correctly." {
  # Execute test
  run echo "${white}white WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;37mwhite WITH space" ]
}
