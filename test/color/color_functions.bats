#!/usr/bin/env bats
load ../helpers/color_helper

# Tests for colors.sh
@test "color::BLACK::Test that BLACK is printed correctly by function." {
  # Execute test
  run BLACK "BLACK WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;30mBLACK WITH space\033[0m" ]
}

@test "color::RED::Test that RED is printed correctly by function." {
  # Execute test
  run RED "RED WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;31mRED WITH space\033[0m" ]
}

@test "color::GREEN::Test that GREEN is printed correctly by function." {
  # Execute test
  run GREEN "GREEN WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;32mGREEN WITH space\033[0m" ]
}

@test "color::YELLOW::Test that YELLOW is printed correctly by function." {
  # Execute test
  run YELLOW "YELLOW WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;33mYELLOW WITH space\033[0m" ]
}

@test "color::BLUE::Test that BLUE is printed correctly by function." {
  # Execute test
  run BLUE "BLUE WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;34mBLUE WITH space\033[0m" ]
}

@test "color::MAGENTA::Test that MAGENTA is printed correctly by function." {
  # Execute test
  run MAGENTA "MAGENTA WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;35mMAGENTA WITH space\033[0m" ]
}

@test "color::CYAN::Test that CYAN is printed correctly by function." {
  # Execute test
  run CYAN "CYAN WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;36mCYAN WITH space\033[0m" ]
}

@test "color::WHITE::Test that WHITE is printed correctly by function." {
  # Execute test
  run WHITE "WHITE WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[0;37mWHITE WITH space\033[0m" ]
}
@test "color::black::Test that black is printed correctly by function." {
  # Execute test
  run black "black WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;30mblack WITH space\033[0m" ]
}

@test "color::red::Test that red is printed correctly by function." {
  # Execute test
  run red "red WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;31mred WITH space\033[0m" ]
}

@test "color::green::Test that green is printed correctly by function." {
  # Execute test
  run green "green WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;32mgreen WITH space\033[0m" ]
}

@test "color::yellow::Test that yellow is printed correctly by function." {
  # Execute test
  run yellow "yellow WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;33myellow WITH space\033[0m" ]
}

@test "color::blue::Test that blue is printed correctly by function." {
  # Execute test
  run blue "blue WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;34mblue WITH space\033[0m" ]
}

@test "color::magenta::Test that magenta is printed correctly by function." {
  # Execute test
  run magenta "magenta WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;35mmagenta WITH space\033[0m" ]
}

@test "color::cyan::Test that cyan is printed correctly by function." {
  # Execute test
  run cyan "cyan WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;36mcyan WITH space\033[0m" ]
}

@test "color::white::Test that white is printed correctly by function." {
  # Execute test
  run white "white WITH space"
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "\033[1;37mwhite WITH space\033[0m" ]
}
