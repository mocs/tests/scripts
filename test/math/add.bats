#!/usr/bin/env bats
load ../helpers/math_helper

@test "math::add::Testing 'add' 0+0=0" {
  run add 0 0
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" -eq 0 ]
}

@test "math::add::Testing 'add' 2+3=5" {
  run add 2 3
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" -eq 5 ]
}

@test "math::add::Testing 'add' 1.4+8.9=10.300" {
  run add 1.4 8.9
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "10.300" ]
}

@test "math::add::Testing 'add' 1.40+8.90=10.300" {
  run add 1.40 8.90
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "10.300" ]
}

@test "math::add::Testing 'add' 1.401+8.902=10.303" {
  run add 1.401 8.902
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "10.303" ]
}
