#!/usr/bin/env bats
load ../helpers/math_helper

@test "math::divide::Testing 'divide' 12/3=4" {
  run divide 12 3
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" -eq 4 ]
}

@test "math::divide::Testing 'divide' 3.4/1.3=2.615" {
  run divide 3.4 1.3
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "2.615" ]
}
