#!/usr/bin/env bats
load ../helpers/math_helper

@test "math::multiply::Testing 'multiply' 4*3=12" {
  run multiply 4 3
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" -eq 12 ]
}

@test "math::multiply::Testing 'multiply' 3.4*1.3=4.420" {
  run multiply 3.4 1.3
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "4.420" ]
}
