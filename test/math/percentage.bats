#!/usr/bin/env bats
load ../helpers/math_helper

# Tests for math.sh percentage function.

@test "math::percentage::Test 1/10 = 10.00." {
  # Execute test
  run percentage 1 10
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "10.00" ]
}

@test "math::percentage::Test percentage 1/100." {
  # Execute test
  run percentage 1 100
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "1.00" ]
}

@test "math::percentage::Test percentage 1/1000." {
  # Execute test
  run percentage 1 1000
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "0.10" ]
}

@test "math::percentage::Test percentage 1/100000." {
  # Execute test
  run percentage 1 100000
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "0.00" ]
}

@test "math::percentage::Test percentage 1/3." {
  # Execute test
  run percentage 1 3
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "33.33" ]
}

@test "math::percentage::Test percentage 2/3." {
  # Execute test
  run percentage 2 3
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "66.67" ]
}

@test "math::percentage::Test percentage 1/1." {
  # Execute test
  run percentage 1 1
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "100.00" ]
}

@test "math::percentage::Test percentage 35/22." {
  # Execute test
  run percentage 35 22
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "159.09" ]
}

@test "math::percentage::Test percentage 1/0." {
  # Execute test
  run percentage 1 0
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "NaN" ]
}

@test "math::percentage::Test percentage 1/0.00000000000000." {
  # Execute test
  run percentage 1 0.00000000000000
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "NaN" ]
}

@test "math::percentage::Test percentage 1/-0.00000000000000." {
  # Execute test
  run percentage 1 -0.00000000000000
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "NaN" ]
}

@test "math::percentage::Test percentage 1/0.000000000000001." {
  # Execute test
  run percentage 1 0.000000000000001
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "100000000000000000.00" ]
}

@test "math::percentage::Test percentage 1.2214/3.56466" {
  # Execute test
  run percentage 1.2214 3.56466
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "34.26" ]
}

@test "math::percentage::Test percentage 23,546/8,654." {
  # Execute test
  run percentage 23,546 8,654
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "272.08" ]
}

@test "math::percentage::Test percentage -1/10." {
  # Execute test
  run percentage -1 10
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "-10.00" ]
}

@test "math::percentage::Test percentage 1/-100." {
  # Execute test
  run percentage 1 -100
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "-1.00" ]
}

@test "math::percentage::Test percentage -1/1000." {
  # Execute test
  run percentage -1 1000
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "-0.10" ]
}

@test "math::percentage::Test percentage 1/-100000." {
  # Execute test
  run percentage 1 -100000
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "-0.00" ]
}

@test "math::percentage::Test percentage -1/3." {
  # Execute test
  run percentage -1 3
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "-33.33" ]
}

@test "math::percentage::Test percentage 2/-3." {
  # Execute test
  run percentage 2 -3
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "-66.67" ]
}

@test "math::percentage::Test percentage -1/1." {
  # Execute test
  run percentage -1 1
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "-100.00" ]
}

@test "math::percentage::Test percentage 35/-22." {
  # Execute test
  run percentage 35 -22
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "-159.09" ]
}

@test "math::percentage::Test percentage -1.2214/3.56466" {
  # Execute test
  run percentage -1.2214 3.56466
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "-34.26" ]
}

@test "math::percentage::Test percentage 23,546/-8,654." {
  # Execute test
  run percentage 23,546 -8,654
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "-272.08" ]
}

@test "math::percentage::Test percentage bad data 1/s." {
  # Execute test
  run percentage 1 s
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "N/A" ]
}

@test "math::percentage::Test percentage bad data s/1." {
  # Execute test
  run percentage s 1
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "N/A" ]
}

@test "math::percentage::Test percentage bad data 1..0/32." {
  # Execute test
  run percentage 1..0 32
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "N/A" ]
}

@test "math::percentage::Test percentage bad data 1/3..2." {
  # Execute test
  run percentage 1 3..2
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "N/A" ]
}

@test "math::percentage::Test percentage bad data 1,,0/3." {
  # Execute test
  run percentage 1,,0 3
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "N/A" ]
}

@test "math::percentage::Test percentage bad data 1/6,,5." {
  # Execute test
  run percentage 1 6,,5
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "N/A" ]
}
