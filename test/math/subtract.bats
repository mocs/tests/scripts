#!/usr/bin/env bats
load ../helpers/math_helper

@test "math::subtract::Testing 'subtract' 0-0=0" {
  run subtract 0 0
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" -eq 0 ]
}
