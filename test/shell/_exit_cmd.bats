#!/usr/bin/env bats
load ../helpers/shell_helper

@test "shell::_exit_cmd::Testing that '_exit_cmd' works with exit code 0." {
  run _exit_cmd "Exiting with no error. Weird." 0
  expected=$(printf "${RED}Fail
${BLUE}Exiting with no error. Weird.
${red}Exiting.${NC}
")
  echo "expected = '${expected[@]}'"
  echo "Output   = '${output[@]}'"
  echo "Status   = '${status}'"
  [ "$status" -eq 0 ]
  [ "$output" = "$expected" ]
}

@test "shell::_exit_cmd::Testing that '_exit_cmd' works with exit code 1." {
  run _exit_cmd "I failed. One time." 1
  expected=$(printf "${RED}Fail
${BLUE}I failed. One time.
${red}Exiting.${NC}
")
  echo "expected = '${expected[@]}'"
  echo "Output   = '${output[@]}'"
  echo "Status   = '${status}'"
  [ "$status" -eq 1 ]
  [ "$output" = "$expected" ]
}

@test "shell::_exit_cmd::Testing that '_exit_cmd' works with exit code 2." {
  run _exit_cmd "I failed. A second time." 2
  expected=$(printf "${RED}Fail
${BLUE}I failed. A second time.
${red}Exiting.${NC}
")
  echo "expected = '${expected[@]}'"
  echo "Output   = '${output[@]}'"
  echo "Status   = '${status}'"
  [ "$status" -eq 2 ]
  [ "$output" = "$expected" ]
}
