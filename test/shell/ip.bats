#!/usr/bin/env bats
load ../helpers/shell_helper

@test "shell::get_local_ip::Testing that 'get_local_ip' gives a valid IPv4 address." {
  run get_local_ip
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [[ "$output" =~ ((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4} ]]
}

@test "shell::get_local_ip::Testing that 'get_local_ip' only gives one ip address." {
  run get_local_ip
  echo "Output = '$output'"
  result=($( echo "$output"))
  echo "Found ${#result[@]} ip-addresses."
  [ "$status" -eq 0 ]
  [[ "${#result[@]}" -eq 1 ]]
}
