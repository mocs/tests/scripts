#!/usr/bin/env bats
load ../helpers/shell_helper

@test "shell::random_string::Testing 'random_string'" {
  run random_string
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "${#output}" -eq 32 ]
  [[ "$output" =~ ([A-Za-z0-9]{32}) ]]
}

@test "shell::random_string::Testing 'random_string' 1" {
  run random_string 1
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "${#output}" -eq 1 ]
  [[ "$output" =~ ([A-Za-z0-9]{1}) ]]
}

@test "shell::random_string::Testing 'random_string' 2" {
  run random_string 2
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "${#output}" -eq 2 ]
  [[ "$output" =~ ([A-Za-z0-9]{2}) ]]
}

@test "shell::random_string::Testing 'random_string' 4" {
  run random_string 4
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "${#output}" -eq 4 ]
  [[ "$output" =~ ([A-Za-z0-9]{4}) ]]
}

@test "shell::random_string::Testing 'random_string' 8" {
  run random_string 8
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "${#output}" -eq 8 ]
  [[ "$output" =~ ([A-Za-z0-9]{8}) ]]
}

@test "shell::random_string::Testing 'random_string' 16" {
  run random_string 16
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "${#output}" -eq 16 ]
  [[ "$output" =~ ([A-Za-z0-9]{16}) ]]
}

@test "shell::random_string::Testing 'random_string' 32" {
  run random_string 32
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "${#output}" -eq 32 ]
  [[ "$output" =~ ([A-Za-z0-9]{32}) ]]
}

@test "shell::random_string::Testing 'random_string' 64" {
  run random_string 64
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "${#output}" -eq 64 ]
  [[ "$output" =~ ([A-Za-z0-9]{64}) ]]
}

@test "shell::random_string::Testing 'random_string' 128" {
  run random_string 128
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "${#output}" -eq 128 ]
  [[ "$output" =~ ([A-Za-z0-9]{128}) ]]
}

@test "shell::random_string::Testing 'random_string' 192" {
  run random_string 192
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "${#output}" -eq 192 ]
  [[ "$output" =~ ([A-Za-z0-9]{192}) ]]
}

@test "shell::random_string::Testing 'random_string' 1219" {
  run random_string 1219
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  echo "${#output}"
  [ "${#output}" -eq 1219 ]
  [[ "$output" =~ ([A-Za-z0-9]) ]]
}
