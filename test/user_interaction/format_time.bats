#!/usr/bin/env bats
load ../helpers/user_interaction_helper

# Tests for user_interaction.sh

@test "user_interaction::format_time::Test that full date with milliseconds is printed correctly." {
  # Execute test
  run format_time 1562149261.950
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "18080 d 10 h 21 m 1 s 950 ms" ]
}

@test "user_interaction::format_time::Test that full date without milliseconds is printed correctly." {
  # Execute test
  run format_time 1562149261.000
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "18080 d 10 h 21 m 1 s 000 ms" ]
}

@test "user_interaction::format_time::Test that time without days is printed correctly." {
  # Execute test
  run format_time 67387.783
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "18 h 43 m 7 s 783 ms" ]
}

@test "user_interaction::format_time::Test that time without hours is printed correctly." {
  # Execute test
  run format_time 2845.462
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "47 m 25 s 462 ms" ]
}

@test "user_interaction::format_time::Test that time without minutes is printed correctly." {
  # Execute test
  run format_time 57.201
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "57 s 201 ms" ]
}

@test "user_interaction::format_time::Test that time without seconds is printed correctly." {
  # Execute test
  run format_time 0.332
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "0 s 332 ms" ]
}

@test "user_interaction::format_time::Test bad data." {
  # Execute test
  run format_time 380jnfs3
  echo "Output = '$output'"
  [ "$status" -eq 0 ]
  [ "$output" = "N/A" ]
}
